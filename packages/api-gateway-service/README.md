# api-gateway-service

## Setup

cp config/example.gateway.config.yml config/gateway.config.yml

## Generate Local Public / Private Key

`openssl genrsa -out private.pem 2048`
`openssl rsa -in private.pem -outform PEM -pubout -out public.pem`
