const axios = require("axios");

const { models } = require("../models");

const { create, getByUsername } = require("./user-service");

jest.mock("axios");

describe("userService", () => {
  describe("getByUsername", () => {
    test("successfully create user", async () => {
      const found = await create({
        username: "username",
        firstName: "Alexander",
        lastName: "Ovechkin",
        password: "password"
      });

      expect(found.status).toBe(200);

      await models.User.destroy({
        where: {
          username: "username"
        }
      });
    });
  });

  describe("getByUsername", () => {
    beforeAll(async () => {
      await models.User.create({
        username: "username"
      });
    });

    afterAll(async () => {
      await models.User.destroy({
        where: {
          username: "username"
        }
      });
    });

    test("username exists", async () => {
      const found = await getByUsername("username");

      expect(found.status).toBe(200);
    });

    test("username does not exists", async () => {
      const found = await getByUsername("username-does-not-exist");

      expect(found.status).toBe(400);
    });
  });
});
