const axios = require("axios");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const { apiGateway } = require("config");

const { models } = require("../models");

const SALT_ROUNDS = 10;

async function create({ username, firstName, lastName, password }) {
  const found = await models.User.findOne({
    where: {
      username
    }
  });

  if (found) {
    return {
      status: 400,
      message: "Username already exists"
    };
  }

  const encrypted = await bcrypt.hash(password, SALT_ROUNDS);

  const result = await models.User.create({
    username,
    firstName,
    lastName,
    password: encrypted
  });

  const tempUser = { ...result.dataValues, password: undefined };

  try {
    await axios.post(`${apiGateway}/users`, tempUser);

    return {
      status: 200,
      data: tempUser
    };
  } catch (err) {
    console.log(err);
    if (err.response) {
      console.log(err.response.status, err.response.body);
    }

    return {
      status: 500,
      message: "Unable to register user"
    };
  }
}

async function getByUsername(username) {
  const found = await models.User.findOne({
    where: {
      username
    }
  });

  if (!found) {
    return {
      status: 400,
      message: "User not found"
    };
  }

  return {
    status: 200,
    data: found
  };
}

async function checkCredentials({ username, password }) {
  const { status, data: found } = await getByUsername(username);

  if (status !== 200 || !found) {
    return {
      status: 400,
      message: "Username/Password incorrect"
    };
  }

  const isValid = await bcrypt.compare(password, found.password);
  if (!isValid) {
    return {
      status: 400,
      message: "Username/Password incorrect"
    };
  }

  return {
    status: 200,
    data: { ...found, password: undefined }
  };
}

async function login({ username, password }) {
  try {
    const credentialStatus = await checkCredentials({ username, password });

    if (credentialStatus.status !== 200) {
      return credentialStatus;
    }

    const { data } = await axios.post(`${apiGateway}/credentials`, {
      consumerId: username,
      type: "jwt"
    });
    const { keyId, keySecret } = data;

    const payload = {
      sub: keyId
    };
    const options = {
      issuer: "react-ecosystem",
      audience: "react-ecosystem"
    };

    const token = jwt.sign(payload, "localhost", options);

    return {
      status: 200,
      data: { token }
    };
  } catch (err) {
    console.log(err);
    return {
      status: 500,
      message: "Something went wrong"
    };
  }
}

module.exports = {
  create,
  getByUsername,
  login,
  checkCredentials
};
