const { schema, migrationPath } = require("config");

const { down, remove } = require("@ether/db-utils");

const { sequelize } = require("./models/index");

const { migrations } = require(`${process.cwd()}/${migrationPath}`);

module.exports = async () => {
  await down({ schema, sequelize, migrations });
  await remove({ schema, sequelize });
};
