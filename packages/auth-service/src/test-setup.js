const { schema, migrationPath } = require("config");

const { remove, init, up } = require("@ether/db-utils");

const { sequelize } = require("./models/index");

const { migrations } = require(`${process.cwd()}/${migrationPath}`);

module.exports = async () => {
  await remove({ schema, sequelize });
  await init({ schema, sequelize });
  await up({ schema, sequelize, migrations });
};
