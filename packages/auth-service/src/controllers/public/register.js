const axios = require("axios").default;
const jwt = require("jsonwebtoken");

const { create } = require("../../services/user-service");

module.exports = function() {
  async function POST(req, res, next) {
    try {
      const { body } = req;
      const { username, firstName, lastName, password } = body;

      const { status, message } = await create({
        username,
        firstName,
        lastName,
        password
      });

      if (status !== 200) {
        res.status(status).json({ message });
        return;
      }

      res.status(200).json({});
    } catch (err) {
      if (err.response) {
        console.log(err.response.status, err.response.body);
      }

      res.status(500).json({ err });
    }
  }

  // NOTE: We could also use a YAML string here.
  POST.apiDoc = {
    summary: "Register a new user",
    parameters: [
      {
        in: "body",
        name: "body",
        required: true,
        schema: {
          $ref: "#/definitions/Registration"
        }
      }
    ],
    responses: {
      200: {
        description: "A list of worlds that match the requested name.",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/World"
          }
        }
      },
      default: {
        description: "An error occurred",
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  return {
    POST
  };
};
