const { login } = require("../../services/user-service");

module.exports = function() {
  async function POST(req, res, next) {
    try {
      const { body } = req;
      const { username, password } = body;

      const { status, data, message } = await login({ username, password });

      if (status !== 200) {
        res.status(status).json({ message });
        return;
      }

      res.status(status).json(data);
    } catch (err) {
      if (err.response) {
        console.log(err.response.status, err.response.body);

        if (err.response.status === 422) {
          res.status(400).json({ message: "Username/Password incorrect" });
        }
      }

      res.status(500).json({ err });
    }
  }

  // NOTE: We could also use a YAML string here.
  POST.apiDoc = {
    summary: "Log the user in",
    parameters: [
      {
        in: "body",
        name: "body",
        required: true,
        schema: {
          $ref: "#/definitions/Login"
        }
      }
    ],
    responses: {
      200: {
        description: "A list of worlds that match the requested name.",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/World"
          }
        }
      },
      default: {
        description: "An error occurred",
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  return {
    POST
  };
};
