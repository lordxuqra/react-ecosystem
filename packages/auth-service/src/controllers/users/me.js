const { getByUsername } = require("../../services/user-service");

const { hasUser } = require("@ether/server-utils");

module.exports = function() {
  async function GET(req, res, next) {
    const { status, data } = await getByUsername(req.user.username);

    res.status(status).json(data);
  }

  // NOTE: We could also use a YAML string here.
  GET.apiDoc = {
    summary: "User Information for current user",
    responses: {
      200: {
        description: "The current logged in user's information",
        schema: {
          type: "array",
          items: {
            $ref: "#/definitions/User"
          }
        }
      },
      default: {
        description: "An error occurred",
        schema: {
          additionalProperties: true
        }
      }
    }
  };

  return {
    GET: hasUser(GET)
  };
};
