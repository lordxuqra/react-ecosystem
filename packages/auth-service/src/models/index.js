const { dbConnection, schema } = require("config");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConnection, { logging: false, schema });

const models = {
  User: sequelize.import("./User.js")
};

Object.values(models).forEach(model => {
  if (typeof model.associate === "function") {
    model.associate(models);
  }
});

module.exports = {
  models,
  sequelize
};
