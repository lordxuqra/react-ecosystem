const tableName = "user";

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        field: "user_id",
        primaryKey: true,
        autoIncrement: true
      },
      username: {
        type: DataTypes.STRING(50),
        field: "username"
      },
      firstName: { type: DataTypes.STRING(50), field: "first_name" },
      lastName: { type: DataTypes.STRING(50), field: "last_name" },
      password: { type: DataTypes.STRING(100), field: "password" }
    },
    { tableName, timestamps: false }
  );

  User.associate = models => {};

  return User;
};
