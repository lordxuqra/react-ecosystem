const Umzug = require("umzug");
const path = require("path");
const { sequelize } = require("../../src/models");

const migrations = new Umzug({
  storage: "sequelize",
  storageOptions: {
    sequelize
  },
  migrations: {
    path: __dirname,
    pattern: /\d+[-\w]+\.js$/,
    params: [sequelize.getQueryInterface(), sequelize.constructor]
  }
});

module.exports = {
  migrations
};
