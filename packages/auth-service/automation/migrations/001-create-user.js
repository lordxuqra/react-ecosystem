const { schema } = require("config");

const tableName = "user";

const up = async (queryInterface, DataTypes) => {
  await queryInterface.createTable(
    { tableName, schema },
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: "user_id"
      },
      username: {
        type: DataTypes.STRING(50),
        field: "username",
        unique: true
      },
      firstName: { type: DataTypes.STRING(50), field: "first_name" },
      lastName: { type: DataTypes.STRING(50), field: "last_name" },
      password: { type: DataTypes.STRING(100), field: "password" }
    }
  );
};

const down = async queryInterface => {
  await queryInterface.dropTable({ tableName, schema });
};

module.exports = {
  up,
  down
};
