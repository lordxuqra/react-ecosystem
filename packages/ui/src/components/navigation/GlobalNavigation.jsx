import React, { Fragment, useContext } from "react";

import {
  Navbar,
  NavBrand,
  NavbarContent,
  NavbarGroup,
  NavItem,
  NavDropdown,
  NavDropdownItem,
  NavDropdownDivider
} from "@ether/components";

import AuthContext from "../../contexts/AuthContext";

function GlobalNavigation() {
  const { user, logout } = useContext(AuthContext);

  return (
    <Navbar hasBottomMargin>
      <NavBrand>Update</NavBrand>
      <NavbarContent>
        {user && (
          <Fragment>
            <NavbarGroup>
              <NavItem to="/home" isActive>
                Home
              </NavItem>
              <NavItem to="/timeline">Timeline</NavItem>
            </NavbarGroup>
            <NavbarGroup tagName="div" isRight>
              <NavDropdown
                label={`${user.firstName || ""} ${user.lastName || ""}`}
              >
                <NavDropdownItem label="Profile" to="#" />
                <NavDropdownDivider />
                <NavDropdownItem label="Logout" onClick={logout} />
              </NavDropdown>
            </NavbarGroup>
          </Fragment>
        )}
      </NavbarContent>
    </Navbar>
  );
}

export default GlobalNavigation;
