import React, { useContext } from "react";
import { Switch, Route, withRouter } from "react-router-dom";

import AuthContext from "../../contexts/AuthContext";

const AuthWall = ({ children }) => {
  const { isLoading } = useContext(AuthContext);

  if (isLoading) {
    return null;
  }

  return (
    <Route
      render={() => {
        return <Switch>{children}</Switch>;
      }}
    />
  );
};

export default withRouter(AuthWall);
