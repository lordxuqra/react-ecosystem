import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import querystring from "querystring";
import Cookies from "js-cookie";

import AuthContext from "../../contexts/AuthContext";

import { getUser } from "../../utils/api/auth";

const AuthWall = ({ children, history, location }) => {
  const [token, setToken] = useState(Cookies.get("token"));

  const [{ isLoading, user }, setUser] = useState({
    isLoading: true,
    user: undefined
  });
  useEffect(() => {
    const _getUser = async () => {
      if (token) {
        const { status, data: apiUser } = await getUser();

        if (status !== 200) {
          setUser({ isLoading: false, user: undefined });
        }

        setUser({ isLoading: false, user: apiUser });
      } else {
        if (!location.search) {
          setUser({ isLoading: false, user: undefined });
          history.push({
            pathname: "/login",
            search: querystring.stringify({
              originalUrl: `${location.pathname}${location.search}`
            })
          });
        }
      }
    };

    _getUser();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  const logout = () => {
    setToken(undefined);
    Cookies.remove("token");
    setUser({ isLoading: false, user: undefined });
  };

  return (
    <AuthContext.Provider value={{ setToken, logout, isLoading, user }}>
      {children}
    </AuthContext.Provider>
  );
};

export default withRouter(AuthWall);
