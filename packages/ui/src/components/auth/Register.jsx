import React, { useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";

import { Alert, Row, Col, Button, InputField } from "@ether/components";

import { register } from "../../utils/api/auth";

function Register({ history }) {
  const [error, setError] = useState();

  return (
    <Row>
      <Col className="col-lg-4 col-md-6 col-8 offset-lg-4 offset-md-3 offset-2">
        <Alert show={error} type="danger">
          {error}
        </Alert>
        <Formik
          initialValues={{
            username: "",
            firstName: "",
            lastName: "",
            password: "",
            confirmPassword: ""
          }}
          validationSchema={Yup.object().shape({
            username: Yup.string().required(),
            firstName: Yup.string().required(),
            lastName: Yup.string().required(),
            password: Yup.string().required(),
            confirmPassword: Yup.string().required()
          })}
          onSubmit={async ({ username, firstName, lastName, password }) => {
            const { status, message } = await register({
              username,
              firstName,
              lastName,
              password
            });

            if (status !== 200) {
              setError(message);
            } else {
              setError();
              history.push("/login");
            }
          }}
          render={() => {
            return (
              <Form>
                <Row>
                  <Col className="col-6">
                    <InputField name="username" label="Username" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <InputField name="firstName" label="First Name" />
                  </Col>
                  <Col>
                    <InputField name="lastName" label="Last Name" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <InputField
                      name="password"
                      label="Password"
                      type="password"
                    />
                  </Col>
                  <Col>
                    <InputField
                      name="confirmPassword"
                      label="Confirm Password"
                      type="password"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button className="btn-primary">Register</Button>
                    <Button to="/login" className="btn-link">
                      Cancel
                    </Button>
                  </Col>
                </Row>
              </Form>
            );
          }}
        />
      </Col>
    </Row>
  );
}

export default Register;
