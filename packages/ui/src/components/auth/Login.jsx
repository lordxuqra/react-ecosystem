import React, { useState, useContext } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import querystring from "querystring";

import { Alert, Row, Col, Button, InputField } from "@ether/components";

import AuthContext from "../../contexts/AuthContext";

import { login } from "../../utils/api/auth";

function Login({ history, location }) {
  const [error, setError] = useState();
  const { isLoading, user, setToken } = useContext(AuthContext);

  if (isLoading) {
    return null;
  }

  if (user) {
    const { originalUrl = "/home" } = querystring.parse(
      location.search.substring(1)
    );
    history.push(originalUrl);
  }

  return (
    <Row>
      <Col className="col-lg-2 col-md-4 col-8 offset-lg-5 offset-md-4 offset-2">
        <Alert show={error} type="danger">
          {error}
        </Alert>
        <Formik
          initialValues={{ username: "", password: "" }}
          validationSchema={Yup.object().shape({
            username: Yup.string().required(),
            password: Yup.string().required()
          })}
          onSubmit={async ({ username, password }, { resetForm }) => {
            resetForm();

            const { status, message, token } = await login({
              username,
              password
            });

            if (status !== 200) {
              setError(message);
            } else {
              setError();
              setToken(token);
            }
          }}
          render={() => {
            return (
              <Form>
                <Row>
                  <Col>
                    <InputField name="username" label="Username" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <InputField
                      name="password"
                      label="Password"
                      type="password"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button className="btn-primary">Log In</Button>
                    <Button className="btn-link" to="/register">
                      Register
                    </Button>
                  </Col>
                </Row>
              </Form>
            );
          }}
        />
      </Col>
    </Row>
  );
}

export default Login;
