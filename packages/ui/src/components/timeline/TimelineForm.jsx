import React, { useMemo } from "react";
import { Form } from "formik";

import { Button, SelectField, DateField } from "@ether/components";

export default ({ dataPoints, types }) => {
  const allowedTypes = useMemo(() => {
    if (!dataPoints.length) {
      return types.initial.map(type => {
        const lookup = types[type];
        return {
          label: lookup.label,
          value: type
        };
      });
    }

    const { followups } = dataPoints[dataPoints.length - 1];
    return followups.map(type => {
      const lookup = types[type];
      return {
        label: lookup.label,
        value: type
      };
    });
  }, [dataPoints, types]);

  return (
    <Form>
      <SelectField
        name="assessmentType"
        label="Assessment Type"
        options={allowedTypes}
      />
      <DateField name="date" label="Date" />
      <Button className="btn-primary">Submit</Button>
    </Form>
  );
};
