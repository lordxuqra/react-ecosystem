import React, { useMemo, useState } from "react";
import { Formik } from "formik";
import moment from "moment";
import * as Yup from "yup";

import { Row, Col, Timeline } from "@ether/components";
import TimelineForm from "./TimelineForm";

const ADMISSION = "admission";
const DISCHARGE = "discharge";
const EXPIRED = "expired";

const types = {
  [ADMISSION]: {
    label: "Admission",
    color: "blue",
    followups: [DISCHARGE, EXPIRED]
  },
  [DISCHARGE]: {
    label: "Discharge",
    color: "orange",
    followups: [ADMISSION]
  },
  [EXPIRED]: {
    label: "Death",
    color: "red",
    followups: []
  },
  initial: [ADMISSION]
};

export default () => {
  const [dataPoints, setDataPoints] = useState([
    {
      id: "2018-03-01",
      x: "2018-03-01",
      ...types[ADMISSION]
    },
    {
      id: "2018-03-15",
      x: "2018-03-15",
      ...types[DISCHARGE]
    },
    {
      id: "2018-11-01",
      x: "2018-11-01",
      ...types[ADMISSION]
    }
  ]);

  const start = useMemo(() => {
    const assessmentPeriod = moment().subtract(24, "months");

    if (!dataPoints.length) {
      return assessmentPeriod.format("YYYY-MM-DD");
    }

    const { x } = dataPoints[0];
    return moment(x).isBefore(assessmentPeriod)
      ? x
      : assessmentPeriod.format("YYYY-MM-DD");

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const end = useMemo(() => moment().format("YYYY-MM-DD"), []);

  return (
    <>
      <Row>
        <Col className="offset-3">
          <Timeline
            {...{
              workingWidth: 1045,
              start,
              end,
              dataPoints
            }}
          />
        </Col>
      </Row>
      <Row>
        <Col className="col-2 offset-5">
          <Formik
            initialValues={{ date: "", assessmentType: "" }}
            validationSchema={Yup.object().shape({
              date: Yup.string().required(),
              assessmentType: Yup.string().required()
            })}
            onSubmit={({ assessmentType, date }) => {
              const type = types[assessmentType];
              setDataPoints([...dataPoints, { id: date, x: date, ...type }]);
            }}
            render={() => {
              return <TimelineForm {...{ types, dataPoints }} />;
            }}
          />
        </Col>
      </Row>
    </>
  );
};
