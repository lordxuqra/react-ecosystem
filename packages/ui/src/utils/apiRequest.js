import axios from "axios";
import Cookies from "js-cookie";

const apiHost = "http://localhost:8080/";

function urlBuilder({ service, isPublic, path }) {
  return `${apiHost}${service}${isPublic ? "-public" : ""}${path}`;
}

export default async ({ method = "GET", service, isPublic, path, options }) => {
  try {
    const token = Cookies.get("token");

    const { status, data } = await axios({
      method,
      url: urlBuilder({ service, isPublic, path }),
      ...options,
      headers: {
        ...(token ? { Authorization: `Bearer ${token}` } : {})
      }
    });

    return { status, data };
  } catch (err) {
    if (err.response) {
      const {
        status,
        data: { message }
      } = err.response;

      return { status, message };
    }

    console.log(err);
    return { status: 500, message: "Unknown Error" };
  }
};
