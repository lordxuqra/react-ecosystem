import Cookies from "js-cookie";

import apiRequest from "../apiRequest";

export const login = async ({ username, password }) => {
  const { status, data, message } = await apiRequest({
    method: "post",
    service: "auth",
    isPublic: true,
    path: "/login",
    options: {
      data: { username, password }
    }
  });

  if (status !== 200) {
    return { status, message };
  }

  const { token } = data;

  Cookies.set("token", token);

  return {
    status,
    token
  };
};

export const register = async ({ username, firstName, lastName, password }) => {
  return await apiRequest({
    method: "post",
    service: "auth",
    isPublic: true,
    path: "/register",
    options: {
      data: { username, firstName, lastName, password }
    }
  });
};

export const getUser = async () => {
  return await apiRequest({
    method: "get",
    service: "auth",
    path: "/users/me"
  });
};
