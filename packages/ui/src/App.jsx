import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import AuthProvider from "./components/auth/AuthProvider";

import GlobalNavigation from "./components/navigation/GlobalNavigation";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import AuthWall from "./components/auth/AuthWall";

import Home from "./components/home/Home";
import TimelineView from "./components/timeline/TimelineView";

function App() {
  return (
    <BrowserRouter>
      <AuthProvider>
        <header>
          <GlobalNavigation />
        </header>
        <main className="App">
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <AuthWall>
              <Route path="/home" component={Home} />
              <Route path="/timeline" component={TimelineView} />
              <Redirect to="/home" />
            </AuthWall>
          </Switch>
        </main>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
