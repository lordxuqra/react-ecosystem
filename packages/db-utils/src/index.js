module.exports = {
  init({ sequelize, schema }) {
    return sequelize.createSchema(schema);
  },
  up({ migrations }) {
    return migrations.up();
  },
  down({ migrations }) {
    return migrations.down();
  },
  remove({ sequelize, schema }) {
    return sequelize.dropSchema(schema);
  }
};
