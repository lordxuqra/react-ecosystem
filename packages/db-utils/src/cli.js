#!/usr/bin/env node
const Sequelize = require("sequelize");

const { dbConnection, schema, migrationPath } = require("config");

const dbHelpers = require("./index");

const { migrations } = require(`${process.cwd()}/${migrationPath}`);
const sequelize = new Sequelize(dbConnection, { logging: false, schema });

// TODO: improve cli experience
const [, , command, ...args] = process.argv;

if (!command) {
  console.log("Error, no argument provided");
  process.exit(1);
}

const runCommand = async internalCommand => {
  try {
    switch (internalCommand) {
      case "help": {
        console.log("---- help coming soon ----");
        break;
      }
      case "restart": {
        await runCommand("remove");
        await runCommand("init");
        await runCommand("up");
        break;
      }
      default: {
        if (dbHelpers[internalCommand]) {
          dbHelpers[internalCommand]({ sequelize, schema, migrations });
        } else {
          console.log("Error, no matching command");
          process.exit(1);
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
};

runCommand(command);
