const axios = require("axios");

const { apiGateway } = require("config");

const hasUser = api => async (req, res, next) => {
  try {
    const egConsumerId = req.headers["eg-consumer-id"];
    console.log(egConsumerId);

    const { status, data } = await axios.get(
      `${apiGateway}/users/${egConsumerId}`
    );
    const { username } = data;

    req.user = {
      username
    };
  } catch (err) {
    console.log(err);
  }

  api(req, res, next);
};

const hasPriv = (privs, api) => (req, res, next) => {};

module.exports = { hasUser, hasPriv };
