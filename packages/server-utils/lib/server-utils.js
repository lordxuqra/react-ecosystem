const { registerService } = require("./register-service");
const { hasUser, hasPriv } = require("./auth-middleware");

module.exports = {
  hasUser,
  hasPriv,
  registerService
};
