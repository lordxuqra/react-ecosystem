import React, { Fragment, useState } from "react";
import { LiveProvider, LiveEditor, LiveError, LivePreview } from "react-live";
import { Formik, Form } from "formik";
import * as Yup from "yup";

import * as scope from "../../lib";

const { Row, Col, Card, CardBody } = scope;

const code = `
// import React, { Fragment } from "react";

// const [showDanger, setShowDanger] = useState(true);
// const [showWarning, setShowWarning] = useState(true);

<Fragment>
  <Alert type='info' show>Info Message</Alert>
  <Alert type='success' show>Success Message</Alert>
  <Alert type='danger' show>Danger Message</Alert>
  <Alert type='warning' show>Warning Message</Alert>
  <Alert type='danger' title="Error Title" show={showDanger} dismiss={() => setShowDanger(!showDanger)}>Message with title</Alert>
  <Alert type='warning' show={showWarning} dismiss={() => setShowWarning(!showWarning)}>Dismissible Message</Alert>
</Fragment>
`;

export default () => {
  const [showDanger, setShowDanger] = useState(true);
  const [showWarning, setShowWarning] = useState(true);

  return (
    <Fragment>
      <Row>
        <Col>
          <h2>Alerts</h2>
        </Col>
      </Row>
      <Row hasBottomMargin>
        <LiveProvider
          {...{
            code,
            scope: {
              ...scope,
              Formik,
              Form,
              Yup,
              Fragment,
              showDanger,
              setShowDanger,
              showWarning,
              setShowWarning
            }
          }}
        >
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col>
                    <LiveEditor />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <LiveError />
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col>
            <Card>
              <CardBody>
                <LivePreview />
              </CardBody>
            </Card>
          </Col>
        </LiveProvider>
      </Row>
    </Fragment>
  );
};
