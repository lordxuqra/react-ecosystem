import React, { Fragment } from "react";
import { LiveProvider, LiveEditor, LiveError, LivePreview } from "react-live";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import moment from "moment";

import * as scope from "../../lib";

const { Row, Col, Card, CardBody } = scope;

const config = {
  workingWidth: 1045,
  start: "2019-01-01",
  end: moment().format("YYYY-MM-DD"),
  dataPoints: [
    { id: 1, x: "2019-03-01", color: "blue" },
    { id: 2, x: "2019-03-15", color: "red" },
    { id: 3, x: "2019-11-01", color: "orange" }
  ]
};

const code = `
// import React from "react";

<Timeline {...${JSON.stringify(config, null, 2)}} />
`;

export default () => (
  <Fragment>
    <Row>
      <Col>
        <h2>Timeline</h2>
      </Col>
    </Row>
    <Row hasBottomMargin>
      <LiveProvider
        {...{ code, scope: { ...scope, Formik, Form, Yup, Fragment } }}
      >
        <Col>
          <Row hasBottomMargin>
            <Col>
              <Card>
                <CardBody>
                  <LivePreview />
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col>
                      <LiveEditor />
                    </Col>
                    <Col>
                      <LiveError />
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Col>
      </LiveProvider>
    </Row>
  </Fragment>
);
