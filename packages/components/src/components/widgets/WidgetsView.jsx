import React from "react";

import { Container, Row, Col } from "../../lib";

import AlertView from "./AlertView";
import TimelineView from "./TimelineView";

export default () => (
  <Container>
    <Row>
      <Col>
        <AlertView />
        <TimelineView />
      </Col>
    </Row>
  </Container>
);
