import React, { Fragment } from "react";
import { LiveProvider, LiveEditor, LiveError, LivePreview } from "react-live";
import { Formik, Form } from "formik";
import * as Yup from "yup";

import * as scope from "../../lib";

const { Row, Col, Card, CardBody } = scope;

const code = `
// import React, { Fragment } from "react";

<Fragment>
  <Row hasBottomMargin>
    <Col>
      <Button className='btn-primary'>Button Primary</Button>
    </Col>
    <Col>
      <Button className='btn-primary' disabled>Disabled Button Primary</Button>
    </Col>
  </Row>
  <Row hasBottomMargin>
    <Col>
      <Button className='btn-secondary'>Button Secondary</Button>
    </Col>
    <Col>
      <Button className='btn-secondary' disabled>Disabled Button Secondary</Button>
    </Col>
  </Row>
  <Row hasBottomMargin>
    <Col>
      <Button className='btn-link' to={{ pathname: '/help' }}>Button Link</Button>
    </Col>
    <Col>
      <Button className='btn-link' disabled>Disabled Button Link</Button>
    </Col>
  </Row>
</Fragment>
`;

export default () => (
  <Fragment>
    <Row>
      <Col>
        <h2>Buttons</h2>
      </Col>
    </Row>
    <Row hasBottomMargin>
      <LiveProvider
        {...{ code, scope: { ...scope, Formik, Form, Yup, Fragment } }}
      >
        <Col>
          <Card>
            <CardBody>
              <Row>
                <Col>
                  <LiveEditor />
                </Col>
              </Row>
              <Row>
                <Col>
                  <LiveError />
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
        <Col>
          <Card>
            <CardBody>
              <LivePreview />
            </CardBody>
          </Card>
        </Col>
      </LiveProvider>
    </Row>
  </Fragment>
);
