import React from "react";

import { Container, Row, Col } from "../../lib";

import ButtonView from "./ButtonView";
import CheckboxFieldView from "./CheckboxFieldView";
import CheckboxGroupFieldView from "./CheckboxGroupFieldView";
import InputFieldView from "./InputFieldView";
import DateFieldView from "./DateFieldView";
import RadioFieldView from "./RadioFieldView";
import SelectFieldView from "./SelectFieldView";

export default () => (
  <Container>
    <Row>
      <Col>
        <ButtonView />
        <CheckboxFieldView />
        <CheckboxGroupFieldView />
        <InputFieldView />
        <DateFieldView />
        <RadioFieldView />
        <SelectFieldView />
      </Col>
    </Row>
  </Container>
);
