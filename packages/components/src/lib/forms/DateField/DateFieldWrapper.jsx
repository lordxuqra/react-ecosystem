import React from "react";

import FieldWrapper from "../FieldWrapper";
import DateFieldComponent from "./DateFieldComponent";

const DateFieldWrapper = ({
  id,
  name,
  label,
  value,
  helpText,
  errors,
  touched,
  className,
  onChange,
  onBlur
}) => {
  const hasErrors = touched && errors && errors.length > 0;
  return (
    <FieldWrapper {...{ id, label, helpText, errors, touched }}>
      <DateFieldComponent
        {...{
          id,
          name,
          value,
          hasHelpText: !!helpText,
          hasErrors,
          className,
          onChange,
          onBlur
        }}
      />
    </FieldWrapper>
  );
};

export default DateFieldWrapper;
