import React from "react";
import classnames from "classnames";
import moment from "moment";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const InputFieldComponent = ({
  id,
  name,
  value,
  hasHelpText,
  hasErrors,
  className,
  onChange,
  onBlur
}) => (
  <div>
    <DatePicker
      id={id}
      name={name}
      onChange={date => {
        const mDate = moment(date);
        const value = mDate.isValid() ? mDate.format("YYYY-MM-DD") : date;

        onChange({ target: { name, value }, type: "change" });
      }}
      onBlur={onBlur}
      className={classnames("form-control", className, {
        "is-invalid": hasErrors
      })}
      selected={value ? moment(value, "YYYY-MM-DD").toDate() : ""}
      dateFormat="yyyy-MM-dd"
      aria-describedby={classnames({
        [`${id}-help`]: hasHelpText,
        [`${id}-errors`]: hasErrors
      })}
    />
  </div>
);

export default InputFieldComponent;
