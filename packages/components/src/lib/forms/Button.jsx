import React from "react";
import { Link } from "react-router-dom";
import classnames from "classnames";

export default ({
  className,
  type,
  onClick,
  children,
  to,
  disabled,
  btn = true
}) => {
  const styles = classnames(className, { btn });

  if (!disabled && to) {
    return <Link {...{ onClick, to, className: styles }}>{children}</Link>;
  }

  return (
    <button {...{ type, onClick, disabled, className: styles }}>
      {children}
    </button>
  );
};
