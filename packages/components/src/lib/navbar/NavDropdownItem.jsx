import React from "react";

import Button from "../forms/Button";

export default ({ label, to, onClick }) => (
  <Button className="dropdown-item" {...{ to, onClick, btn: false }}>
    {label}
  </Button>
);
