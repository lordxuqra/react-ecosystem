import React from "react";
import classnames from "classnames";

import Button from "../forms/Button";

export default ({ tagName = "li", isActive, children, to = "#", onClick }) => {
  const Tag = tagName;
  return (
    <Tag className={classnames("nav-item", { active: isActive })}>
      <Button className="nav-link" {...{ to, onClick, btn: false }}>
        {children} {isActive && <span className="sr-only">(current)</span>}
      </Button>
    </Tag>
  );
};
