import React, { useMemo } from "react";
import PropTypes from "prop-types";
import moment from "moment";

const DataPoint = ({
  id,
  x,
  color = "black",
  start,
  workingWidth,
  totalMarkers
}) => {
  const diff = moment
    .duration(moment(x, "YYYY-MM-DD").diff(start, "YYYY-MM-DD"))
    .asMonths();

  const center = diff ? workingWidth * (diff / totalMarkers) : 0;

  return (
    <>
      <rect width={1} height={5} y={0} fill={color} x={center} />
      <circle cx={center} cy={10} r={4} stroke={color} fill="#FFF" />
    </>
  );
};

const Timeline = ({ dataPoints, workingWidth = 475, start, end }) => {
  const totalMarkers = useMemo(() => {
    const diff = moment.duration(
      moment(end, "YYYY-MM-DD").diff(start, "YYYY-MM-DD")
    );

    return diff.asMonths();
  }, [start, end]);

  const markers = Array.from({ length: totalMarkers }, (v, i) => (
    <rect
      width={1}
      height={5}
      y={0}
      fill={"#000000"}
      x={(workingWidth / (totalMarkers - 1)) * i}
      key={i}
    />
  ));

  const props = {
    start,
    workingWidth,
    totalMarkers
  };

  return (
    <div style={{ height: "40px" }}>
      <svg width="100%" height="100%">
        <g transform={`translate(10,10)`}>
          <rect width={workingWidth} height={1} />
          <DataPoint {...{ x: start, ...props }} />
          {dataPoints.map(dp => (
            <DataPoint {...{ ...dp, ...props }} key={dp.id} />
          ))}
          <DataPoint {...{ x: end, ...props }} />
          {markers}
        </g>
      </svg>
    </div>
  );
};

Timeline.propTypes = {
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  workingWidth: PropTypes.number,
  dataPoints: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      x: PropTypes.string,
      color: PropTypes.string
    })
  ).isRequired
};

export default Timeline;
