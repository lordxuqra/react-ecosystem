import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const Alert = ({ children, type, title, show, dismiss }) => (
  <div
    className={classnames("alert", {
      [`alert-${type}`]: true,
      "d-none": !show
    })}
  >
    {title && (
      <h5>
        {title}
        {dismiss && (
          <button
            type="button"
            className="close"
            aria-label="Close"
            onClick={dismiss}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        )}
      </h5>
    )}

    {!title && dismiss && (
      <button
        type="button"
        className="close"
        aria-label="Close"
        onClick={dismiss}
      >
        <span aria-hidden="true">&times;</span>
      </button>
    )}
    {children}
  </div>
);

Alert.propTypes = {
  type: PropTypes.oneOf(["info", "success", "danger", "warning"]),
  title: PropTypes.string,
  show: PropTypes.bool,
  dismiss: PropTypes.func
};

Alert.defaultProps = {
  title: undefined,
  show: false,
  dismiss: undefined
};

export default Alert;
